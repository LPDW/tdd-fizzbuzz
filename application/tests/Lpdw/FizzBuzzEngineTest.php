<?php

namespace Lpdw;


use PHPUnit\Framework\TestCase;

class FizzBuzzEngineTest extends TestCase
{
    private $fizzBuzzEngine;

    /**
     * @before
     */
    public function init()
    {
        $this->fizzBuzzEngine = new FizzBuzzEngine();
    }

    /**
     * @test
     */
    public function should_return_1_on_1()
    {
        $answer = $this->fizzBuzzEngine->say(1);
        self::assertEquals("1", $answer);
    }
    /**
     * @test
     */
    public function should_return_2_on_2()
    {
        $answer = $this->fizzBuzzEngine->say(2);
        self::assertEquals("2", $answer);
    }

    /**
     * @test
     */
    public function should_return_fizz_on_3()
    {
        $answer = $this->fizzBuzzEngine->say(3);
        self::assertEquals(FizzBuzzEngine::FIZZ, $answer);
    }

    /**
     * @test
     */
    public function should_return_buzz_on_5()
    {
        $answer = $this->fizzBuzzEngine->say(5);
        self::assertEquals(FizzBuzzEngine::BUZZ, $answer);
    }

    /**
     * @test
     */
    public function should_return_fizz_on_6()
    {
        $answer = $this->fizzBuzzEngine->say(6);
        self::assertEquals(FizzBuzzEngine::FIZZ, $answer);
    }

    /**
     * @test
     */
    public function should_return_bang_on_7()
    {
        $answer = $this->fizzBuzzEngine->say(7);
        self::assertEquals(FizzBuzzEngine::BANG, $answer);
    }

    /**
     * @test
     */
    public function should_return_buzz_on_10()
    {
        $answer = $this->fizzBuzzEngine->say(10);
        self::assertEquals(FizzBuzzEngine::BUZZ, $answer);
    }

    /**
     * @test
     */
    public function should_return_fizzbuzz_on_15()
    {
        $answer = $this->fizzBuzzEngine->say(15);
        self::assertEquals(FizzBuzzEngine::FIZZBUZZ, $answer);
    }

}