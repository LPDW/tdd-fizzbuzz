<?php


namespace Lpdw;


class FizzBuzzEngine
{
    const FIZZ = "fizz";
    const BUZZ = "buzz";
    const BANG = "bang";
    const FIZZBUZZ = "fizzbuzz";

    const RULES = [
        15 => self::FIZZBUZZ,
        7 => self::BANG,
        5 => self::BUZZ,
        3 => self::FIZZ,

    ];

    /**
     * FizzBuzzEngine constructor.
     */
    public function __construct()
    {
    }

    public function say(int $number)
    {
        $answer = $this->selectRightAnswer($number);

        return $answer;
    }

    public function selectRightAnswer(int $number):string
    {
        foreach(self::RULES as $divisor => $value) {
            if ($this->isNumberMultipleOf($divisor, $number)) {
                return self::RULES[$divisor];
            }
        }

        return $number;
    }

    protected function isNumberMultipleOf(int $divisor, int $number):bool
    {
        return $number % $divisor == 0;
    }
}